import { NgModule } from '@angular/core';
import { LibComponent } from './lib.component';
import { MotorsComponent } from './components/motors/motors.component';
import { CommonModule } from '@angular/common';
import { AlertsComponent } from './components/alerts/alerts.component';
import { ReportsComponent } from './components/reports/reports.component';

@NgModule({
  declarations: [
    LibComponent,
    MotorsComponent,
    AlertsComponent,
    ReportsComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    LibComponent,
    MotorsComponent,
    AlertsComponent
  ]
})
export class LibModule { }
