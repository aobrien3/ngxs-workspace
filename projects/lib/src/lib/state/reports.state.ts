import { Injectable } from "@angular/core";
import { Action, NgxsOnInit, Selector, State, StateContext } from "@ngxs/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { LoadReports } from "../actions/reports.actions";
import { Report } from "../models/report.model";
import { ReportsService } from "../services/reports.service";

export interface ReportsStateModel{
  items: Report[];
}


@State<ReportsStateModel>({
  name: 'reports',
  defaults: {
    items:[]
  }
})

@Injectable()
export class ReportsState implements NgxsOnInit{
  constructor(private reportsService : ReportsService){}

  @Selector()
  static getReports(ctx:ReportsStateModel){
    return ctx.items;
  }

  ngxsOnInit(ctx: StateContext<Report[]>){
    ctx.dispatch(new LoadReports());
  }

  @Action(LoadReports)
  loadReports(ctx: StateContext<ReportsStateModel>, action:LoadReports): Observable<Report[]>{
    return this.reportsService.loadReports().pipe(tap(d => ctx.patchState({items:d})))
  }
}
