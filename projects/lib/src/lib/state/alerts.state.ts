import { Injectable } from "@angular/core";
import { Action, NgxsOnInit, Selector, State, StateContext } from "@ngxs/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { LoadAlerts } from "../actions/alerts.actions";
import { Alert } from "../models/alerts.model";
import { AlertsService } from "../services/alerts.service";

export interface AlertStateModel{
  items: Alert[];
}


@State<AlertStateModel>({
  name: 'alerts',
  defaults: {
    items:[]
  }
})

@Injectable()
export class AlertState implements NgxsOnInit{
  constructor(private alertsService : AlertsService){}

  @Selector()
  static getAlerts(ctx:AlertStateModel){
    return ctx.items;
  }

  ngxsOnInit(ctx: StateContext<Alert[]>){
    ctx.dispatch(new LoadAlerts());
  }

  @Action(LoadAlerts)
  loadAlerts(ctx: StateContext<AlertStateModel>, action:LoadAlerts): Observable<Alert[]>{
    return this.alertsService.loadAlerts().pipe(tap(d => ctx.patchState({ items: d})))
  }
}
