import { Injectable } from "@angular/core";
import { Action, NgxsOnInit, Selector, State, StateContext } from "@ngxs/store";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { LoadMotors } from "../actions/motors.actions";
import { Motor } from "../models/motors.model";
import { MotorsService } from "../services/motors.service";

export interface MotorStateModel{
  items: Motor[];
}

@State<MotorStateModel>({
  name: 'motors',
  defaults: {
    items:[]
  }
})
@Injectable()
export class MotorState implements NgxsOnInit{
  constructor(private motorsService: MotorsService){}

  @Selector()
  static getMotors(ctx:MotorStateModel) {
    return ctx.items;
  }

  ngxsOnInit(ctx: StateContext<Motor[]>){
    ctx.dispatch(new LoadMotors());
  }

  @Action(LoadMotors)
  loadMotors(ctx: StateContext<MotorStateModel>, action: LoadMotors):Observable<Motor[]>{
    return this.motorsService.loadMotors().pipe(tap(d => ctx.patchState({ items: d})))
  }
}
