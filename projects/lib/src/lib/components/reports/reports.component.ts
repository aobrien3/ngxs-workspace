import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Report } from '../../models/report.model';
import { ReportsState } from '../../state/reports.state';

@Component({
  selector: 'lib-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.css']
})
export class ReportsComponent implements OnInit {
  @Select(ReportsState.getReports) reports$: Observable<Report[]>
  constructor() { }

  ngOnInit(): void {
  }

}
