import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Alert } from '../../models/alerts.model';
import { AlertState } from '../../state/alerts.state';
@Component({
  selector: 'lib-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {
  @Select(AlertState.getAlerts) alerts$: Observable<Alert[]>;
  constructor() { }

  ngOnInit(): void {
  }

}
