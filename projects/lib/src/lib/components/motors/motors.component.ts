import { Component, OnInit } from '@angular/core';
import { Select } from '@ngxs/store';
import { Observable } from 'rxjs';
import { Motor } from '../../models/motors.model';
import { MotorState } from '../../state/motor.state';

@Component({
  selector: 'lib-motors',
  templateUrl: './motors.component.html',
  styleUrls: ['./motors.component.scss']
})
export class MotorsComponent implements OnInit {
  @Select(MotorState.getMotors) motors$: Observable<Motor[]>
  constructor() { }

  ngOnInit(): void {
  }

}
