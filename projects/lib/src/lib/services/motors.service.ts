import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Motor } from "../models/motors.model";
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class MotorsService {

  constructor(private http: HttpClient) {}

  public loadMotors(): Observable<any[]> {
    let motors = { "motors" :[
      { "name": "Motor 1", "id": 1 },
      { "name": "PMP-34", "id": 2 },
      { "name": "C1 33", "id": 3 },
      { "name": "C8R 33", "id": 4 },
      { "name": "Motor 5", "id": 5 },
      { "name": "Motor 6", "id": 6 },
      { "name": "Motor 7", "id": 7 },
      { "name": "Motor 8", "id": 8 }
    ]}
    return of(motors).pipe(map(m => m.motors));
  }
}
