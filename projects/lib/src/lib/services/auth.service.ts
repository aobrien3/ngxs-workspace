import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { Auth } from "../models/auth.model";

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(){}

  public login(payload):Observable<any>{
    let user = {
      "username": payload.username,
      "token": Math.floor(Math.random() * 9).toString(),
      "professional": Math.random()
    }
    return of({"token": Math.floor(Math.random() * 9).toString()});
  }

  public logout(token): Observable<any>{
    return of(null);
  }
}
