import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class AlertsService {

  constructor() {}

  public loadAlerts(): Observable<any[]> {
    let alerts = { "alerts" :[
      { "motorName": "Motor 1", "id": 1, "motorComponent": "bearing" },
      { "motorName": "PMP-34", "id": 2, "motorComponent": "alignment"  },
      { "motorName": "C1 33", "id": 3, "motorComponent": "cooling system"  },
      { "motorName": "C8R 33", "id": 4, "motorComponent": "loose foot"  },
      { "motorName": "Motor 5", "id": 5, "motorComponent": "bearing" },
      { "motorName": "Motor 6", "id": 6, "motorComponent": "eccentricity"  },
      { "motorName": "Motor 7", "id": 7, "motorComponent": "bearing"  },
      { "motorName": "Motor 8", "id": 8, "motorComponent": "bearing"  }
    ]}
    return of(alerts).pipe(map(m => m.alerts));
  }
}
