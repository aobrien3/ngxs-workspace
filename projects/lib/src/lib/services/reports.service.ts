import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root',
})
export class ReportsService {

  constructor(private http: HttpClient) {}

  public loadReports(): Observable<any[]> {
    let reports = { "reports" :[
      { "name": "Report 1", "id": 1 },
      { "name": "Report 2", "id": 2 },
      { "name": "Report 3", "id": 3 },
      { "name": "Report 4", "id": 4 },
      { "name": "Report 5", "id": 5 },
      { "name": "Report 6", "id": 6 },
      { "name": "Report 7", "id": 7 },
      { "name": "Report 8", "id": 8 }
    ]}
    return of(reports).pipe(map(m => m.reports));
  }
}
