export interface Auth {
  token: string;
  username: string;
}
