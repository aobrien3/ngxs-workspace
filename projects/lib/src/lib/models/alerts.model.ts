export interface Alert {
  id: string;
  motorName: string;
  motorComponent: string;
}
