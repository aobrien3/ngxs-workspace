import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { OverviewRoutingModule } from './overview-routing.module';
import { SummaryComponent } from './summary/summary.component';
import { NgxsModule } from '@ngxs/store';
import { AlertState, MotorState, ReportsState } from 'lib';

@NgModule({
  declarations: [
    SummaryComponent
  ],
  imports: [
    CommonModule,
    OverviewRoutingModule,
    NgxsModule.forFeature([
      AlertState,
      MotorState,
      ReportsState
    ]),
  ]
})
export class OverviewModule { }
