import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AlertsRoutingModule } from './alerts-routing.module';
import { NgxsModule } from '@ngxs/store';
import { AlertState } from 'lib';

@NgModule({
  declarations: [
  ],
  imports: [
    CommonModule,
    AlertsRoutingModule,
    NgxsModule.forFeature([
      AlertState
    ]),
  ]
})
export class AlertsModule { }
