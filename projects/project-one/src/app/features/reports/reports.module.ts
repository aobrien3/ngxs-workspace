import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportsRoutingModule } from './reports-routing.module';
import { NgxsModule } from '@ngxs/store';
import { ReportsState } from 'lib';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReportsRoutingModule,
    NgxsModule.forFeature([
      ReportsState
    ]),
  ]
})
export class ReportsModule { }
