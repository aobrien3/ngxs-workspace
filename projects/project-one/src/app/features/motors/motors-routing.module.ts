import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MotorsComponent } from 'lib';

const routes: Routes = [
  {path: '', pathMatch: 'full', component: MotorsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MotorsRoutingModule { }
