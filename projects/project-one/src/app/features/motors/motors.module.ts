import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MotorsRoutingModule } from './motors-routing.module';
import { MotorState} from 'lib';
import { NgxsModule } from '@ngxs/store';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MotorsRoutingModule,
    NgxsModule.forFeature([
      MotorState
    ]),
  ]
})

export class MotorsModule {

}
