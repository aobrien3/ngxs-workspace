import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from 'lib';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './public/login/login.component';
import { PageComponent } from './shared/page/page.component';

const routes: Routes = [
  { path:'login', component: LoginComponent},
  { path: '', component: PageComponent, children: [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]},
    {
      path: 'motors',
      loadChildren: () => import('./features/motors/motors.module').then(m => m.MotorsModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'alerts',
      loadChildren: () => import('./features/alerts/alerts.module').then(m => m.AlertsModule),
      canActivate: [AuthGuard]
    },
    {
      path: 'reports',
      loadChildren: () => import('./features/reports/reports.module').then(m => m.ReportsModule),
      canActivate: [AuthGuard]
    },
    { path: '',   redirectTo: '/login', pathMatch: 'full' }, // redirect to `motors`
  ]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
