import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MotorsComponent } from 'lib';

const routes: Routes = [
  { path: 'motors', component: MotorsComponent},
  { path: '',   redirectTo: '/motors', pathMatch: 'full' }, // redirect to `motors`
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
